// quanto tempo manca al prossimo natale con qualce abbellimento

function calcoloGiorni (time) {
    return Math.floor(time/1000/60/60/24);
}

var Natale = new Date(2017,11,25);
var tempo = Date.now() - Natale.getTime();
var mancano = calcoloGiorni(tempo);
switch (true) {
    case mancano > 0:
        console.log("Il Natale è passato da " + Math.abs(mancano) +" giorni");
        break;
    case mancano < 0:
        console.log("Al Natale mancano "+ Math.abs(mancano) +" giorni");
        break;
    case mancano === 0:
        console.log("Oggi è Natale !!!!!");
        break;
    default:
        console.log("errore !!!!");
}