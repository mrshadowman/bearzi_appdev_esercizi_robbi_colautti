//Visualizzazione data di oggi

var data = new Date();
var arrSettimane=["Domenica","Lunedì","Martedì","Mercoledì","Gioved","Venerdì","Sabato"];
var arrMesi=["Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Lugio","Agosto","Settembre","Ottobre","Novembre","Dicembre"];
var ora = data.getHours();
if ( ora>12 ) {
    ora=ora-12;
    ampm=" del pomeriggio"
} else {
    ampm=" del mattino"
}
console.log("Oggi è "+ arrSettimane[data.getDay()] +", "+ data.getDate()+" "+ arrMesi[data.getMonth()] + " "+ data.getFullYear() +". Ora sono le "+ ora +" e "+ data.getMinutes() + ampm);