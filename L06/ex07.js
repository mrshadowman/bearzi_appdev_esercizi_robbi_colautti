// conteggio vocali in una frase

function conteggio_vocali(Stringa) {
    var stringa = Stringa.split("");
    var num_vocali = 0;
    for (var i=0; i<stringa.length; i++) {
        switch (stringa[i]) {
            case "a":
                num_vocali++;
                break;
            case "e":
                num_vocali++;
                break;
            case "i":
                num_vocali++;
                break;
            case "o":
                num_vocali++;
                break;
            case "u":
                num_vocali++;
                break;
        }
    }
    return num_vocali;
}

var Frase ="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";
console.log("il numero di vocali presenti nella frase è : "+conteggio_vocali(Frase));