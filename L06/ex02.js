// area triangolo

function area_triangolo(base,altezza){
    if ( base==0 && altezza==0 ) {
        return 0;
    }
    return base*altezza/2;
}

console.log("calcolo area di un triangolo base 5, altezza 7 = "+area_triangolo(5,7));
console.log("----------------------------------------------------------");
console.log("calcolo area di un triangolo base 5, altezza 5 = "+area_triangolo(5,5));
console.log("----------------------------------------------------------");
console.log("calcolo area di un triangolo base 515, altezza 77 = "+area_triangolo(515,77));




