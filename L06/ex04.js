// numero random tra un minimo e un massimo

function Random(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
var numeri_cardinali =["Primo","Secondo","Terzo","Quarto","Quinto","Sesto","Settimo", "Ottavo", "Nono","Decimo"];
console.log("calcolo di 10 numeri casuali compresi tra 0 e 100 ");
for (var i=0; i<10; i++) {
    console.log ( "Il "+ numeri_cardinali[i] +" numero è: "+Random(0,100));
}