/* funzione ottimizzata per implementare l'ereditarietà */
var __extends = (this && this.__extends) || (function () {
   var extendStatics = Object.setPrototypeOf ||
       ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
       function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
   return function (d, b) {
       extendStatics(d, b);
       function __() { this.constructor = d; }
       d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
   };
})();


// creazione classe persona

var Persona = (function () {
   function Persona(nome, anni) {
       this.nome = nome;
       this.eta = anni;
   }
   Persona.prototype.getProfilo = function () {
        out = this.nome+" e una persona di anni "+this.eta;
        return out;
   };
   return Persona;
}());

/* estende Person */
var Atleta = (function (_super) {
   __extends(Atleta, _super);
   function Atleta(nome, eta, velocita) {
       var _this = _super.call(this, nome, eta) || this;
       _this.velocita = velocita;
       return _this;
   }
   /* sovrascrive il metodo aggiungendone funzionalità */
   Atleta.prototype.getProfilo = function () {
       var out = _super.prototype.getProfilo.call(this);
       out += " , ha una velocita di "+this.velocita+" m/s";
       return out;
   };
   Atleta.prototype.corri = function (secondi,stato) {
        var distanza = 0;
        var velocitacorsa = this.velocita;
        if (stato === "ideale") {
        distanza = secondi*velocitacorsa;
        }
         else {
            while (secondi > 0) {
                var secUtil = 10/velocitacorsa;
                if (secUtil>=secondi) {
                        distanza += velocitacorsa/secondi;
                break;
                }
                else {
                    distanza += 10;
                    secondi -= secUtil;
                    velocitacorsa = velocitacorsa - this.eta*0.002;
                }
            }
        }
   }
   return Atleta;
}(Persona));

var concorrente1 = new Atleta ("Roberto", 25, 8);
var concorrente2 = new Atleta ("Mario ", 45, 8);
var concorrente3 = new Atleta ("Gianni", 55, 8);

console.log ("L'Atleta "+concorrente1.nome+" di "+concorrente1.eta+" anni in 60 secondi ha percorso la distanza di "+Math.round(concorrente1.corri(60))+" metri");
console.log ("L'Atleta "+concorrente2.nome+" di "+concorrente2.eta+" anni in 60 secondi ha percorso la distanza di "+Math.round(concorrente2.corri(60))+" metri");
console.log ("L'Atleta "+concorrente3.nome+" di "+concorrente3.eta+" anni in 60 secondi ha percorso la distanza di "+Math.round(concorrente3.corri(60))+" metri");
