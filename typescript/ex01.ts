class Persona {
    nome: string;
    eta: number;

    constructor (nome, eta) {
        this.nome = nome;
        this.eta = eta;
    }

    getProfile() : string
    {
        return this.nome+" è una persona di "+this.eta+" anni";
    }

}

class Atleta extends Persona {
    velocita: number;

    constructor (nome, eta, speed) {
        super(nome, eta);
        this.velocita = speed;
    }

    getProfile() {

    }

}