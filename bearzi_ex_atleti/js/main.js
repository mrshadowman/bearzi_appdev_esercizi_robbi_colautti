var Persona = (function () {
    function Persona(name, age) {
        this.nome = name;
        this.eta = age;
    }
    Persona.prototype.getProfile = function () {
        return this.nome+" è una persona di "+this.eta+" anni";
    };
    return Persona;
}());

var Atleta = (function (_super) {
    __extends(Atleta, _super);
    function Atleta(name, age, speed,ginfo,gcors, gnome, gmetri) {
        var _this = _super.call(this, name, age) || this;
        _this.velocita = speed;
        _this.gara_info = ginfo;
        _this.gara_corri_nome = gnome;
        _this.gara_corri_metri = gmetri;
        _this.gara_corsia = gcors;
        return _this;
    }

    Atleta.prototype.metri_percorsi_totali = 0;
    Atleta.prototype.gara_info = {};
    Atleta.prototype.gara_corsia = {};
    Atleta.prototype.gara_corri_nome = {};
    Atleta.prototype.gara_corri_metri = {};

    Atleta.prototype.getProfile = function () {
        var out = _super.prototype.getProfile.call(this);
        out += " che corre a "+this.velocita+" m/s";
        return out;
    };

    Atleta.prototype.corri = function (secondi) {
        var metri_percorsi_adesso = this.velocita*secondi;
        var perdita_mt = 0;
        var distanza = 0;
        while (distanza<=metri_percorsi_adesso) {
            perdita_mt += (distanza/10)/100*(0.2*this.eta);
            distanza += 10;
        }
        this.metri_percorsi_totali += metri_percorsi_adesso - Math.round(perdita_mt);
    };

    return Atleta;
}(Persona));

// creo tre istanze di Atleta e scrivo i lalori di default
var atleta_1 = new Atleta("Giovanni",36,6,$("#descrizioni").find(".atleta1") , $("#gara").find(".atleta1") , $(".atleta1 div.nome") , $(".atleta1 div.metri"));
atleta_1.gara_info.html(atleta_1.getProfile());
atleta_1.gara_corri_nome.html(atleta_1.nome);
atleta_1.gara_corri_metri.html("0 mt");

var atleta_2 = new Atleta("Andrea",29,7,$("#descrizioni").find(".atleta2") , $("#gara").find(".atleta2") , $(".atleta2 .nome") , $(".atleta2 .metri"));
atleta_2.gara_info.html(atleta_2.getProfile());
atleta_2.gara_corri_nome.html(atleta_2.nome);
atleta_2.gara_corri_metri.html("0 mt");

var atleta_3 = new Atleta("Anna",18,8,$("#descrizioni").find(".atleta3") , $("#gara").find(".atleta3") , $(".atleta3 .nome") , $(".atleta3 .metri"));
atleta_3.gara_info.html(atleta_3.getProfile());
atleta_3.gara_corri_nome.html(atleta_3.nome);
atleta_3.gara_corri_metri.html("0 mt");

var pulsante = $("#start input");

// gara
var aggiorna = function () {
    atleta_1.corri(10);
    atleta_2.corri(10);
    atleta_3.corri(10);

    atleta_1.gara_corri_metri.html(atleta_1.metri_percorsi_totali+" mt");
    atleta_1.gara_corsia.css({ width: (atleta_1.metri_percorsi_totali/500*100)+"%" });
    atleta_2.gara_corri_metri.html(atleta_2.metri_percorsi_totali+" mt");
    atleta_2.gara_corsia.css({ width: (atleta_2.metri_percorsi_totali/500*100)+"%" });
    atleta_3.gara_corri_metri.html(atleta_3.metri_percorsi_totali + " mt");
    atleta_3.gara_corsia.css({width: (atleta_3.metri_percorsi_totali / 500 * 100) + "%"});

    if (atleta_1.metri_percorsi_totali>500) {
        atleta_1.gara_corri_metri.html("500 mt");
        atleta_1.gara_corsia.css({ width: "100%", background: "red" });
        pulsante.off("click");
        return;
    }

    if (atleta_2.metri_percorsi_totali>500) {
        atleta_2.gara_corri_metri.html("500 mt");
        atleta_2.gara_corsia.css({ width: "100%", background: "red" });
        pulsante.off("click");
        return;
    }

    if (atleta_3.metri_percorsi_totali>500) {
        atleta_3.gara_corri_metri.html("500 mt");
        atleta_3.gara_corsia.css({ width: "100%", background: "red" });
        pulsante.off("click");
        return;
    }

};

pulsante.on("click",aggiorna);

